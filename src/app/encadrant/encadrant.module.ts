import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { EncadrantRoutingModule } from './encadrant-routing.module';
import { EncadrantComponent } from './encadrant.component';
import { InscriptionComponent } from './inscription/inscription.component';


@NgModule({
  declarations: [EncadrantComponent, InscriptionComponent],
  imports: [
    CommonModule,
    EncadrantRoutingModule
  ]
})
export class EncadrantModule { }
