import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EncadrantComponent } from './encadrant.component';
import {InscriptionComponent} from './inscription/inscription.component';
import {StagiaireComponent} from '../stagiaire/stagiaire.component';

const routes: Routes = [{ path: '', component: EncadrantComponent }];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class EncadrantRoutingModule { }
