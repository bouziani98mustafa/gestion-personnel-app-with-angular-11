import { Component, OnInit } from '@angular/core';
import {InscriptionComponent} from '../inscription/inscription.component';
import {MatDialog} from '@angular/material/dialog';
import {ConnexionComponent} from '../connexion/connexion.component';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  constructor(private dialog: MatDialog) {
  }

  ngOnInit(): void {
  }

  openDialogInscription(): void {
    const dialogRef = this.dialog.open(InscriptionComponent, {
      width: '500px',
      data: {}
    });
    }
  openDialogConnexion(){
    const dialogRef = this.dialog.open(ConnexionComponent, {
      width: '400px',
      data: {}
    });
  }

}
