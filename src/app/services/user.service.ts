import { Injectable } from '@angular/core';

@Injectable(
  {providedIn: 'root'}
)
export class UserService {
  emoji = '🐳';
  constructor() { }

  public getString(){
    return this.emoji;
  }
  getConnected(){
    return localStorage.getItem('user');
  }
}
