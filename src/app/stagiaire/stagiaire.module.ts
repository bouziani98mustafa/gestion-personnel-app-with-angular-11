import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { StagiaireRoutingModule } from './stagiaire-routing.module';
import { StagiaireComponent } from './stagiaire.component';


@NgModule({
  declarations: [StagiaireComponent],
  imports: [
    CommonModule,
    StagiaireRoutingModule
  ]
})
export class StagiaireModule { }
