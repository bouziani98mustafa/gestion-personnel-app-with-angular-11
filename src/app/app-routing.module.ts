import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import {AcceuilComponent} from './acceuil/acceuil.component';
import {PreloadAllModules} from '@angular/router';
import {MyGuardGuard} from './my-guard.guard';
const routes: Routes = [
  {path: '',
    component: AcceuilComponent
  },
  { path: 'stagiaire', loadChildren: () => import('./stagiaire/stagiaire.module').then(m => m.StagiaireModule), canActivate: [MyGuardGuard] },
  { path: 'encadrant', loadChildren: () => import('./encadrant/encadrant.module').then(m => m.EncadrantModule) }
];

@NgModule({
  imports: [RouterModule.forRoot(routes,{preloadingStrategy: PreloadAllModules})],
  exports: [RouterModule]
})
export class AppRoutingModule { }
