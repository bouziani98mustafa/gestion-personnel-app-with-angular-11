import {animation, animate, style
} from '@angular/animations';

export const transAnimation = animation([
  style({
    opacity: '{{ opacity }}',
  }),
  animate('{{ time }}')
]);
export const transAnimation2 = animation([
  style({
    marginRight: '{{ marginRight }} ',
  }),
  animate('{{ time }}')
]);
