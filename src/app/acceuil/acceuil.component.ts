import {Component, OnInit, ViewChild} from '@angular/core';
import {transAnimation} from '../Transitions/AcceuilTransition/OpacityTransition';
import {transition, trigger, useAnimation} from '@angular/animations';
import {NgbCarousel, NgbCarouselConfig, NgbSlideEvent, NgbSlideEventSource} from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-acceuil',
  animations: [
    trigger('AugmentOpacity', [
      transition('void => *', [
        useAnimation(transAnimation, {
          params: {
            opacity: 0,
            time: '1.5s'
          }
        })
      ])
    ]),
    trigger('AugmentOpacity2', [
      transition('void => *', [
        useAnimation(transAnimation, {
          params: {
            opacity: 0,
            time: '0.2s 1.5s'
          }
        })
      ])
    ])
  ],
  templateUrl: './acceuil.component.html',
  styleUrls: ['./acceuil.component.css']
})
export class AcceuilComponent implements OnInit {

  constructor(config: NgbCarouselConfig) {
    config.interval = 10000;
    config.wrap = false;
    config.keyboard = false;
    config.pauseOnHover = false;
  }

  ngOnInit(): void {
  }

}
