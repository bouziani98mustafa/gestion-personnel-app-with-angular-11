import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.component.html',
  styleUrls: ['./connexion.component.css']
})
export class ConnexionComponent implements OnInit {

  constructor(private route: Router) { }

  ngOnInit(): void {
  }
  connexion(value: any){
    localStorage.setItem('email', value.nom);
    localStorage.setItem('password', value.password);
  }
}
