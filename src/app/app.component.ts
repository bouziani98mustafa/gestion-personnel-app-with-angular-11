import {Component, Input, OnInit, Output} from '@angular/core';
import {from, fromEvent, interval, Observable} from 'rxjs';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ajax} from 'rxjs/ajax';
import {find} from 'rxjs/operators';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
})
export class AppComponent implements OnInit{
  title = 'GestionDesStagiaires';
  isLinear = false;
  myObsevable = new Observable();
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  constructor(private _formBuilder: FormBuilder) {
    this.firstFormGroup = this._formBuilder.group({
      firstCtrl: ['', Validators.required]
    });
    this.secondFormGroup = this._formBuilder.group({
      secondCtrl: ['', Validators.required]
    });
  }

  callWheneSubscribe(){
    console.log('subscribed');
  }

  ngOnInit(): void {

   this.ObservableFromFrom();
   // this.ObservableFRomInterval();
   //  this.ObservableFromFromEvent();
   //  this.ObservableFRomAjax();
  }

ObservableFromFrom(){
  const data = from(['ayyoub', 'mohamed', 'said']);
  data.pipe(find(val => val.startsWith('ayy'))).subscribe((v) => console.log(`value: ${v}`));

}

ObservableFRomInterval(){
  // Create an Observable that will publish a value on an interval
  const secondsCounter = interval(1000);
// Subscribe to begin publishing values
  const subscription = secondsCounter.subscribe(n =>
    console.log(`It's been ${n + 1} seconds since subscribing!`));
}

ObservableFromFromEvent(){
  const el = document.getElementById('id');

// Create an Observable that will publish mouse movements
  // @ts-ignore
  const mouseMoves = fromEvent( el, 'mouseleave');

// Subscribe to start listening for mouse-move events
  // @ts-ignore
  const subscription = mouseMoves.subscribe((evt: MouseEvent) => {
    // Log coords of mouse movements
    console.log(`Coords: ${evt.clientX} X ${evt.clientY}`);

    // When the mouse is over the upper-left of the screen,
    // unsubscribe to stop listening for mouse movements
    if (evt.clientX < 40 && evt.clientY < 40) {
      subscription.unsubscribe();
    }
  });
}

ObservableFRomAjax(){
// Create an Observable that will create an AJAX request
  const apiData = ajax('https://jsonplaceholder.typicode.com/users');
  // Subscribe to create the request
  return  apiData.subscribe(
    res => console.log(res.status, res.response, res.request, res.responseType),
      error => {console.log('you got an error')}
      );
}
}

