import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.component.html',
  styleUrls: ['./inscription.component.css']
})
export class InscriptionComponent implements OnInit {
  constructor(private route: Router) {
  }
  ngOnInit(): void {

  }
  inscription(value: any){
    localStorage.setItem('user', value);
    console.log(value);
    setTimeout(() => {
      if (value.profession === 'stagiaire'){
        this.route.navigate(['/stagiaire']);
      }
      if (value.profession === 'encadrant'){
        this.route.navigate(['/encadrant']);
      }
    }, 2000);
  }
}


localStorage.setItem('currentUser', this.registerForm.value.firstName);